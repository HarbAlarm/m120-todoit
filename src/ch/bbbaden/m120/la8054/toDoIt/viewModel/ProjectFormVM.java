package ch.bbbaden.m120.la8054.toDoIt.viewModel;

import ch.bbbaden.m120.la8054.toDoIt.Main;
import ch.bbbaden.m120.la8054.toDoIt.model.Project;
import ch.bbbaden.m120.la8054.toDoIt.model.ProjectModel;

public class ProjectFormVM {
    private Main main;
    private ProjectModel model;

    public ProjectFormVM(Main main, ProjectModel model) {
        this.main = main;
        this.model = model;
    }

    public void actSave(String projName) {
        model.addProject(new Project(projName));
        main.closeProjectForm();
    }
    public void actCancel() {
        main.closeProjectForm();
    }
}
