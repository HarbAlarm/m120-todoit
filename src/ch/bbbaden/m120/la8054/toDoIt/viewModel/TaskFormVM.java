package ch.bbbaden.m120.la8054.toDoIt.viewModel;

import ch.bbbaden.m120.la8054.toDoIt.Main;
import ch.bbbaden.m120.la8054.toDoIt.model.Project;
import ch.bbbaden.m120.la8054.toDoIt.model.Task;
import ch.bbbaden.m120.la8054.toDoIt.model.TaskModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class TaskFormVM implements PropertyChangeListener {
    private Main main;
    private TaskModel model;
    private ObservableList<Task> taskList = FXCollections.observableArrayList();
    private Task selectedTask;
    private Project selectedProject;

    public TaskFormVM(Main main, TaskModel model) {
        this.main = main;
        this.model = model;
    }

    public ObservableList getTasks() {
        return taskList;
    }

    public void setSelectedTask(Task newVal) {
        this.selectedTask = newVal;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        switch (evt.getPropertyName()) {
            case "selectedProject":
                this.selectedProject = model.getSelectedProject();
                break;
            case "tasks":
                this.taskList.setAll(model.getAllTasks(selectedProject));
                break;
        }
    }
}
