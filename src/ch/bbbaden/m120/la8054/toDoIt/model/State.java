package ch.bbbaden.m120.la8054.toDoIt.model;

public enum State {
    OPEN,
    DONE,
    CLOSED;
}
