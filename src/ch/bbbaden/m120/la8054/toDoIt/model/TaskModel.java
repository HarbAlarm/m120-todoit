package ch.bbbaden.m120.la8054.toDoIt.model;

import java.beans.PropertyChangeListener;
import java.util.List;

public interface TaskModel {
    public void addPropertyChangeListener(PropertyChangeListener l);
    public void removePropertyChangeListener(PropertyChangeListener l);
    public void addTask(Project p, Task newTask);
    public void editTask(Project p, Task newTask);
    public void removeTask(Project p, Task Task);
    public void deleteAllTasks(Project p);
    public List<Task> getAllTasks(Project p);

    public Project getSelectedProject();
}
