package ch.bbbaden.m120.la8054.toDoIt.view;

import ch.bbbaden.m120.la8054.toDoIt.viewModel.ProjectFormVM;
import javafx.beans.binding.Bindings;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

public class ProjectFormController implements Initializable {
    private ProjectFormVM viewModel;

    @FXML
    private TextField txtProjName;
    @FXML
    private Button btnCancel;
    @FXML
    private Button btnSave;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }

    @FXML
    private void actCancel() {
        viewModel.actCancel();
    }

    @FXML
    private void actSave() {
        viewModel.actSave(txtProjName.getText());
    }


    public void bind() {
        btnSave.disableProperty().bind(Bindings.equal("", txtProjName.textProperty()));
    }

    public void setViewModel(ProjectFormVM vm) {
        this.viewModel = vm;
    }
}
