package ch.bbbaden.m120.la8054.toDoIt.view;

import ch.bbbaden.m120.la8054.toDoIt.model.Project;
import ch.bbbaden.m120.la8054.toDoIt.model.Task;
import ch.bbbaden.m120.la8054.toDoIt.viewModel.ProjectListVM;
import javafx.beans.value.ChangeListener;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;

import java.net.URL;
import java.util.ResourceBundle;

public class ProjectListController implements Initializable {
    private ProjectListVM viewModel;

    @FXML
    private ListView lstProjects;
    @FXML
    private ListView lstTasks;
    @FXML
    private Button btnDelete;
    @FXML
    private Button BtnNew;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }

    @FXML
    private void actDelete() {
        viewModel.actRemove();
    }

    @FXML
    private void actNewProject() {
        viewModel.addProject();
    }

    @FXML
    private void actNewTask() {
        viewModel.addTask();
    }

    public void bind() {
        btnDelete.disableProperty().bind(viewModel.getSelectedProperty());
        lstProjects.setItems(viewModel.getProjects());
        lstProjects.getSelectionModel().selectedItemProperty().addListener((ChangeListener<Project>)(observableValue, oldVal, newVal) -> viewModel.setSelectedProject(newVal));
//        lstTasks.setItems(viewModel.getTasks());
//        lstTasks.getSelectionModel().selectedItemProperty().addListener((ChangeListener<Task>) (observableValue, oldVal, newVal) -> viewModel.setSelectedTask(newVal));
    }

    public void setViewModel(ProjectListVM vm) {
        this.viewModel = vm;
    }
}
