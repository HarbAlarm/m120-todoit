package ch.bbbaden.m120.la8054.toDoIt;

import ch.bbbaden.m120.la8054.toDoIt.model.Model;
import ch.bbbaden.m120.la8054.toDoIt.model.Project;
import ch.bbbaden.m120.la8054.toDoIt.model.Task;
import ch.bbbaden.m120.la8054.toDoIt.view.ProjectFormController;
import ch.bbbaden.m120.la8054.toDoIt.view.ProjectListController;
import ch.bbbaden.m120.la8054.toDoIt.viewModel.ProjectFormVM;
import ch.bbbaden.m120.la8054.toDoIt.viewModel.TaskFormVM;
import ch.bbbaden.m120.la8054.toDoIt.viewModel.ProjectListVM;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

public class Main extends Application {
    protected Stage stage;
    protected Stage formStage;
    protected Stage taskStage = new Stage();
    private Scene scene;
    private Parent root;
    private Parent formRoot;
    protected Model model;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        this.stage = stage;
        this.formStage = new Stage();
        this.model = new Model();

        initList();
        initForm();

        addContent();

        showProjectList();
    }

    public void showProjectList() {
        stage.show();
    }

    public void closeProjectList() {
        stage.close();
    }

    public void showProjectForm(Project selectedProject) {
        formStage.show();
    }

    public void closeProjectForm() {
        formStage.close();
    }

    private void initList() {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("view/ProjectList.fxml"));
        try {
            root = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        ProjectListController view = loader.getController();
        ProjectListVM vm = new ProjectListVM(this, model);
        model.addPropertyChangeListener(vm);

        stage.setTitle("i3Dev");
        stage.setMinHeight(400);
        stage.setMinWidth(600);
        scene = new Scene(root, 600, 400);
        stage.setScene(scene);

        view.setViewModel(vm);
        view.bind();
    }
    private void initForm() {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("view/ProjectForm.fxml"));
        try {
            formRoot = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        ProjectFormController view = loader.getController();
        ProjectFormVM vm = new ProjectFormVM(this, model);

        formStage.setTitle("i3Dev");
        formStage.setMinHeight(100);
        formStage.setMinWidth(200);
        formStage.setScene(new Scene(formRoot, 200, 100));
        formStage.initModality(Modality.WINDOW_MODAL);
        formStage.initOwner(scene.getWindow());

        view.setViewModel(vm);
        view.bind();
    }


    private void addContent() {
        Project p1 = new Project("Beschaffung");
        p1.addTask(new Task("Detailstudie", "Variantenvergleich machen"));
        p1.addTask(new Task("Anforderungen", "Besprechen des Anforderungen"));

        Project p2 = new Project("Benutzerverwaltung");
        p2.addTask(new Task("Projekt Setup", "Projekt erstellen"));
        p2.addTask(new Task("Git", "Versionskontrolle einrichten"));
        p2.addTask(new Task("Architektur", "OOA"));
        p2.addTask(new Task("Design", "OOD"));
        p2.addTask(new Task("Testing", "Unittests erstellen"));
        p2.addTask(new Task("Implementieren", "Klassendiagramm umsetzen"));

        Project p3 = new Project("freeCMS");
        p3.addTask(new Task("Projekt Setup", "Projekt erstellen"));
        p3.addTask(new Task("Architektur", "Aufgaben der Schichten definieren"));
        p3.addTask(new Task("Anforderungen", "Anforderungen aufnehmen"));
        p3.addTask(new Task("Pflichtenheft", "erstellen des Pflichtenhefts"));
        p3.addTask(new Task("Implementieren", "Klassendiagramm umsetzen"));
        model.addProject(p1);
        model.addProject(p2);
        model.addProject(p3);
    }

    public void showTaskForm() {
        taskStage.show();
    }
}
